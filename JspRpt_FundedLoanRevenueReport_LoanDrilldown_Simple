ALTER Procedure [dbo].[JspRpt_FundedLoanRevenueReport_LoanDrilldown_Simple]
	@paraGLBranch	varchar(5)
	, @paraFromDate date
	, @paraEndDate date


AS
	/* ---- LVFundedLoanRevenueReport_Detail  --------------------
	Jasper Report Name: P and L: Funded Loan Revenue - Loan Number Table

	Modified by		Modify Date		Description
	Don Manuel		08/12/2019		Loan View 
									Funded Loan Report

	EXEC JspRpt_FundedLoanRevenueReport_LoanDrilldown_Simple '1032', '01-01-2021','08-31-2021'
	----------------------------------------------------------- */

	BEGIN
		With ResultSet
		AS
		(	Select
				[Loan Number] + ' ' + [Borrower Name] as [Borrower Name],
				GLTYPE,
				[Loan Number],
				[FINSTMTGROUP],
				[Loan Officer],
				[Loan Amount],
				[Investor],
				[Investor Date],
				[Interest Rate],
				[Loan Type],
				[Funding Date],
				SUM(AmountWork) as AmountWork,
				CASE WHEN GroupWork = '04-Branch Margin' then concat(GroupWork, ' - ',[Acct Number]) 
					 ELSE GroupWork END AS GroupWork
			from 
				v_LVFundedLoanPandL	
			where 
				BranchUniqueIdentifier = @paraGLBranch 
				And [Funding Date] between CAST(@paraFromDate as VArchar(10)) and  Cast(@paraEndDate as Varchar(10))
						

			Group By
				[Loan Number] + ' ' + [Borrower Name],
				GLTYPE,
				[Loan Number],
				[FINSTMTGROUP],
				[Loan Officer],
				[Loan Amount],
				[Investor],
				[Investor Date],
				[Interest Rate],
				[Loan Type],
				[Funding Date],
				CASE WHEN GroupWork = '04-Branch Margin' then concat(GroupWork, ' - ',[Acct Number]) 
					 ELSE GroupWork END
		)
	
		Select * from ResultSet Where AmountWork <> 0
		ORDER BY 
				GLTYPE DESC, GroupWork asc
	END
